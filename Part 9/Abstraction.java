/*
Code by: @Dwarka prasad Bairwa
Content :
        - Abstraction
 */

//simple abstract Program
//abstract class
abstract class Part {
    private String identifier;
    private String manufacturer;
    private String description;

    //constructor
    public Part(String identifier, String manufacturer, String description) {
        this.identifier = identifier;
        this.manufacturer = manufacturer;
        this.description = description;
    }
    //Methods
    public String getIdentifier() {
        return identifier;
    }

    public String getDescription() {
        return description;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    //abstract method
    public abstract void print();
}
//inheritance class
class Engine extends Part{

    private String engineType;

    public Engine(String engineType, String identifier, String manufacturer, String description){
        //calling part class constructor
        super(identifier, manufacturer, description);
        this.engineType = engineType;
    }
    //method of Engine class
    public String getEngineType() {
        return engineType;
    }
    //overriding abstract method
    @Override
    public void print(){
        System.out.println(this.engineType + super.getIdentifier() + super.getDescription() + super.getManufacturer());
    }


}

public class Abstraction{
    public static void main(String[] args){
        //creating constructor
        Engine engine = new Engine("combustion", "hz", "volkswagen", "VW GOLF 1L 86-91");
        engine.print();
    }
}