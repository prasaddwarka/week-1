/*
Code by: @Dwarka prasad Bairwa
Content :
        - Interfaces
*/
//creating interface
interface Readable{
    String read();
}

//implementing interfaces
class TextMessage implements Readable{
    private String sender;
    private String content;
    //constructor
    public TextMessage(String sender, String content) {
        this.sender = sender;
        this.content = content;
    }

    public String getSender() {
        return this.sender;
    }

    public String read() {
        return this.content;
    }
}

public class Interfaces{
    public static void main(String[] args){

        //creating object
        TextMessage message = new TextMessage("ope", "It's going great!");
        System.out.println(message.read());
    }
}
