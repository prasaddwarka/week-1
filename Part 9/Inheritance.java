/*
Code by: @Dwarka prasad Bairwa
Content :
        - Inheritance
 */
//simple inheritance Program
class Part {
    private String identifier;
    private String manufacturer;
    private String description;

    //constructor
    public Part(String identifier, String manufacturer, String description) {
        this.identifier = identifier;
        this.manufacturer = manufacturer;
        this.description = description;
    }
    //Methods
    public String getIdentifier() {
        return identifier;
    }

    public String getDescription() {
        return description;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    @Override
    public String toString(){
        return "Idetifier: " + this.identifier + "\n"
                + "Description: "+ this.description + "\n"
                + "Manufacturer: "+ this.manufacturer;
    }
}
//inheritance class
class Engine extends Part{

    private String engineType;

    public Engine(String engineType, String identifier, String manufacturer, String description){
        //calling part class constructor
        super(identifier, manufacturer, description);
        this.engineType = engineType;
    }
    //method of Engine class
    public String getEngineType() {
        return engineType;
    }

    @Override
    public String toString(){
        return "EngineType: "+ this.engineType + "\n"
                + super.toString();
    }
}

public class Inheritance{
    public static void main(String[] args){
        //creating constructor
        Engine engine = new Engine("combustion", "hz", "volkswagen", "VW GOLF 1L 86-91");
        System.out.println(engine);
    }
}