/*
Code by: @Dwarka prasad Bairwa

Content:
        - Basic Java Fundamentals
 */


import java.util.Scanner;

public class Example{
    public static void main(String[] args){


        //printing statements
        System.out.println("Hello World!");
        System.out.println("Welcome to JAVA!");


        //Reading Inputs via scanner
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a message");
        String message = input.nextLine();
        System.out.println(message);



        //creating variables
        //int , double , boolean , string etc.
        String Name = "My Name is Dwarka.";
        int age = 22;
        double height = 5.6;
        boolean student = true;

        System.out.println("Your Name? "+Name);
        System.out.println("Your Age? "+age);
        System.out.println("your Height? "+height);
        System.out.println("Are you a student? "+student);




        //Reading Inputs for different types of Variables
        String Fname;
        int Fage;
        double Fheight;
        boolean Fstudent;

        System.out.println("Enter Friend Name: ");
        Fname = input.nextLine();
        System.out.println("Enter Friend Age: ");
        Fage = input.nextInt(); //we can use Integer.valueof(String)....
        System.out.println("Enter Friend Height: ");
        Fheight = input.nextDouble();
        System.out.println("Is your Friend is a Student? ");
        Fstudent = input.nextBoolean();

        System.out.println("Your Entered details are: " + "\n" + Fname + " "+ Fage+ " "+Fheight +" " +Fstudent);


        //calculation with Numbers
        int One = 1;
        int Two = 2;
        System.out.println("Add " + (One+Two));
        System.out.println("Minus " + (Two-One));
        System.out.println("Multiply " + (One * Two));
        System.out.println("Division " + (Two/One));
        //Type conversion
        double Result;
        System.out.println("Type Converted Result is: "+ ((double)Two/One));


        //conditional operations
        int cAge;
        System.out.println("Enter Your Age: ");
        cAge = input.nextInt();

        if(cAge < 14){
            System.out.println("You are Not Allowed to Work.");
        }
        else if(cAge >= 14 && cAge <18){
            System.out.println("You are Allowed to work in some places.");
        }else{
            System.out.println("you are Free to choose your Work.");
        }


        //comparing Strings
        // equals method etc..
        String text1 = "Dwarka";
        String text2 = "Dwarka";

        if (text1 == text2) {
            System.out.println("The strings were the same!");
        } else {
            System.out.println("The strings were different!");
        }

        //using equals method
        if (text1.equals(text2)) {
            System.out.println("The strings were the same!");
        } else {
            System.out.println("The strings were different!");
        }

    }
}