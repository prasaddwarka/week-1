/*
Code by: @Dwarka prasad Bairwa

Content :
        - Loops and Functions

 */

import java.util.Scanner;

public class Example{

    //method or function

    //simple Method
    public static void SayHello(){
        System.out.println("Hello! Welcome to Greeting Method");
    }

    //parametric method...
    public static void SaySubject(String Subject){
        System.out.println("Hii! Welcome to "+Subject);
    }

    //returning a Value
    public static int Sum(int first,int second){
        return first+second;
    }

    //Method Calling Another Method
    public static void callingSum(){
        int sum = Sum(1,2);
        System.out.println("Again Sum of those Numbers are: "+ sum);
    }

    public static void main(String[] args){

        //calling method
        SayHello();

        //calling Method Via parameter
        SaySubject("JAVA");

        //Accepting Method Value
        int sum = Sum(1,2);
        System.out.println("Sum of input Numbers are: "+ sum);

        //method inside Method
        callingSum();

        Scanner scan = new Scanner(System.in);

        int num=0;
        sum = 0;

        //while loop
        while(num<5){
            sum += num;
            num++;
        }
        System.out.println("Total Sum is: " + sum);

        //For loop
        for(num=0; num < 5; num++) {
            System.out.print(num);
        }


    }
}