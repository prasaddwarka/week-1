/*
Code by: @Dwarka prasad Bairwa

Content :
        - strings

 */
import java.util.Scanner;

public class string{
    public static void main(String[] args){

        Scanner in = new Scanner(System.in);

        //creating a string variable
        String Name="Dwarka";

        //Reading and Printing
        String SurName = in.nextLine();
        System.out.println("Name: " + Name + " SurName: "+ SurName);  //concationation of a string

        //string comparisons
        String AnotherName = "Dwarka";
        if (Name.equals(AnotherName)) {
            System.out.println("Both Name are Same.");
        } else {
            System.out.println("Both Name are Different.");
        }

        //splitting a string
        String text = "first second third fourth";
        String[] pieces = text.split(" "); //string array
        for(String piece:pieces){
            System.out.println(piece);
        }

        //contains method of string
        text = "java";
        if (text.contains("av")) {
            System.out.println("av found");
        }

        //Accessing characters
        text = "Hello world!";
        char character = text.charAt(0);
        System.out.println(character);
    }
}