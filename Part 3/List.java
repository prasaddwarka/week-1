/*
Code by: @Dwarka prasad Bairwa

Content :
        - List

 */

import java.util.ArrayList;
import java.util.Scanner;

public class List{
    public static void main(String[] args){

        //creating a list
        ArrayList<String> list = new ArrayList<>();

        //adding and retrieving a value
        //Retrieve info from a place that doesn't exits gives IndexOutOfBoundsException
        list.add("Dwarka");
        list.add("Prasad");

        System.out.println(list.get(0));
        System.out.println(list.get(1));

        //getting size
        System.out.println("Size of the List is: "+ list.size());

        //iterating over the list
        //simple for loop
        for (int index = 0; index < list.size(); index++) {
            System.out.println(list.get(index));
        }
        //for each loop
        for(String ele:list){
            System.out.println(ele);
        }

        //Removing element From List
        list.add("Bairwa");
        list.remove(0); //index
        list.remove("Prasad"); //by element itself

        for(String ele:list){
            System.out.println(ele);
        }

        //finding a element
        list.add(0,"Dwarka"); //adding on a specific position
        list.add(1,"Prasad");

        if(list.contains("Dwarka")){
            System.out.println("Your Given element found!");
        }
    }
}