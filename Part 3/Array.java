/*
Code by: @Dwarka prasad Bairwa

Content :
        - Array

 */
import java.util.Scanner;

public class Array{
    public static void main(String[] args){

        //creating a list
        int[] arr = new int[3];
        // shorter way to create array is->  int[] num = {1,2};

        //adding and retrieving a value
        //Retrieve info from a place that doesn't exits gives ArrayIndexOutOfBoundsException
        arr[0] = 1;
        arr[1] = 2;

        System.out.println(arr[0]);
        System.out.println(arr[1]);

        //getting size
        System.out.println("Size of the Array is: "+ arr.length);

        //iterating over the list
        //simple for loop
        for (int index = 0; index < arr.length; index++) {
            System.out.println(arr[index]);
        }
        //for each loop
        for(int ele:arr){
            System.out.println(ele);
        }

        //Passing Array To a Method
        printEle(arr);
    }

    //method which Accepts Array as Parameter
    public static void printEle(int[] Arr){
        System.out.println("Inside Method");
        for(int ele:Arr){
            System.out.println(ele);
        }
    }
}
