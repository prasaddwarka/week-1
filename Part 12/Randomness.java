/*
Code by: @Dwarka prasad Bairwa
Content :
        - Randomness
 */
import java.util.Random;

public class Randomness{
    public static void main(String[] args){
        //creating random class object
        Random Num = new Random();

        for(int i=0;i<10;i++){
            System.out.print(Num.nextInt(100) + " ");
        }

        //making whether prediction
        double propability = Num.nextDouble();
        System.out.println();
        if (propability <= 0.1) {
            System.out.println("It rains");
        } else if (propability <= 0.4) { // 0.1 + 0.3
            System.out.println("It snows");
        } else { // rest, 1.0 - 0.4 = 0.6
            System.out.println("The sun shines");
        }

        System.out.println((4 * Num.nextGaussian() - 3));
    }
}
