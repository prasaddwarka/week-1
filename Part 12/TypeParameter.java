/*
Code by: @Dwarka prasad Bairwa
Content :
        - Type Parameter
 */

//Generic class
class Person<T>{
    private T element;

    //generic class constructor
    public Person(T element){
        this.element = element;
    }

    public void printData(){
        System.out.println(this.element);
    }
}

public class TypeParameter{
    public static void main(String[] args) throws Exception{
        //integer type
        Person<Integer> num = new Person<Integer>(5);
        num.printData();

        //String type
        Person<String> Name = new Person<String>("Rahul");
        Name.printData();

    }
}

