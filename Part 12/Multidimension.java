/*
Code by: @Dwarka prasad Bairwa
Content :
        - MultiDimensional Data
 */
public class Multidimension{
    public static void main(String[] args){
        //2d array
        int row = 2;
        int column = 3;

        //creating 2d array
        int[][] TD = new int[row][column];

        System.out.println("row, column, value");
        for (row = 0; row < TD.length; row++) {
            for (column = 0; column < TD[row].length; column++) {
                int value = TD[row][column];
                System.out.println("" + row + ", " + column + ", " + value);
            }
        }

        //Adding Value into 2D Array
        TD[0][1] = 4;
        TD[1][1] = 1;
        TD[1][0] = 8;
        System.out.println("row, column, value");
        for (row = 0; row < TD.length; row++) {
            for (column = 0; column < TD[row].length; column++) {
                int value = TD[row][column];
                System.out.println("" + row + ", " + column + ", " + value);
            }
        }


    }
}