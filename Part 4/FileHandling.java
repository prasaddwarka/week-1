/*
Code by: @Dwarka prasad Bairwa
Content :
        - File Handling
*/
import java.util.Scanner;
import java.nio.file.Paths;

public class FileHandling{
    public static void main(String[] args) throws Exception{

        //reading file
        Scanner sc = new Scanner(Paths.get("sample.txt"));

        //scanning File
        while(sc.hasNextLine()){
            String Line = sc.nextLine();

            //skipping blank line
            if(Line.isEmpty()) continue;

            System.out.println(Line);
        }

    }
}