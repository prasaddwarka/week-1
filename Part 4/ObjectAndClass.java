/*
Code by: @Dwarka prasad Bairwa

Content :
        - Object and class And Object in List

 */
import java.util.ArrayList;

//creating class
class Person{
    //class variables
    private String Name;
    private int Age;

    //constructor (we can call multiple constructor with different parameter)
    public Person(String name,int Age){
        this.Name = name;
        this.Age=Age;
    }

    //method
    public void print(){
        System.out.println("Name: "+ this.Name+"\n"+
                "Age: "+this.Age);
    }

    //changing instance value
    public void growOlder() {
        this.Age = this.Age + 1;
    }

    //String representation of an object
    public String toString(){
        return this.Name + " " + this.Age;
    }

}

public class ObjectAndClass{
    public static void main(String[] args){

        //creating object
        Person person = new Person("Dwraka",18);

        //printing data
        person.print();

        //calling method
        person.growOlder();
        person.print();

        //ToString method
        System.out.println(person);


        /* --Object in List-- */
        ArrayList<Person> persons = new ArrayList<>();

        //adding objects
        persons.add(new Person("Dwarka",20));
        persons.add(new Person("Ravi",21));
        persons.add(new Person("Sohan",18));
        persons.add(new Person("Jay",15));

        //printing all persons
        for(Person ps:persons){
            System.out.println(ps);
        }
    }
}