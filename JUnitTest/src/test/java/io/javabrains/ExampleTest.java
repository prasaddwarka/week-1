package io.javabrains;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExampleTest{

    @Test
    void test(){
        MathUtils mathutils = new MathUtils();
        int expected = 2;
        int actual = mathutils.add(1,1);
        Assertions.assertEquals(expected, actual);
    }
}