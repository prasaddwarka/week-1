/*
Code by: @Dwarka prasad Bairwa
Content :
        - Exception
 */
import java.util.Scanner;
import java.util.ArrayList;
import java.nio.file.Paths;

public class exception{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);

        System.out.print("Give a number: ");
        int readNumber;

        //NumberFormatException
        try {
            readNumber = reader.nextInt();
            System.out.println("Good job!");
        } catch (Exception e) {
            System.out.println("User input was not a number."+ e.getMessage());
        }

        //file reading
        ArrayList<String> lines =  new ArrayList<>();

        try {
            Scanner sc = new Scanner(Paths.get("file.txt"));

            while(sc.hasNext()){
                lines.add(sc.nextLine());
            }
            for(String Line:lines){
                System.out.println(Line);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }
}