/*
Code by: @Dwarka prasad Bairwa
Content :
        - Package
 */
package library;

public class Book{
    private String name;

    public Book(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}