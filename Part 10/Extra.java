/*
Code by: @Dwarka prasad Bairwa
Content :
        - StringBuilder, Regular Expressions
 */
import java.util.*;

public class Extra{
    public static void main(String[] args) {

        /* --String Builder-- */
        //creating String builder object
        StringBuilder numbers = new StringBuilder();

        //appending in stringBuilder
        for (int i = 1; i < 5; i++) {
            numbers.append(i);
        }
        System.out.println(numbers.toString());

        /* --Regular Expressions-- */
        String string = "00";

        if (string.matches("00|111|0000")) {
            System.out.println("The string contained one of the three alternatives");
        } else {
            System.out.println("The string contained none of the alternatives");
        }

        //* in regex
        string = "trolololololo";

        if (string.matches("trolo(lo)*")) {
            System.out.println("Correct form.");
        } else {
            System.out.println("Incorrect form.");
        }
    }
}