/*
Code by: @Dwarka prasad Bairwa
Content :
        - Comparable Interface
 */
//using Comparable interface
//must override compareTo() method
import java.util.*;

class Member implements Comparable<Member> {
    private String name;
    private int height;

    public Member(String name, int height) {
        this.name = name;
        this.height = height;
    }

    public String getName() {
        return this.name;
    }

    public int getHeight() {
        return this.height;
    }

    @Override
    public String toString() {
        return this.getName() + " (" + this.getHeight() + ")";
    }

    @Override
    public int compareTo(Member member) {
        if (this.height == member.getHeight()) {
            return 0;
        } else if (this.height > member.getHeight()) {
            return 1;
        } else {
            return -1;
        }
    }
}

public class comparable{
    public static void main(String[] args){
        //creating object List
        List<Member> member = new ArrayList<>();

        member.add(new Member("mikael", 182));
        member.add(new Member("matti", 187));
        member.add(new Member("ada", 184));

        member.stream().forEach(m -> System.out.println(m));

        //sorting using Stream sort method
        member.stream().sorted().forEach(m -> System.out.println(m));

        //sorting using collection sort
        Collections.sort(member);
        member.stream().forEach(m -> System.out.println(m));

        //using Lamba Function to sort accroding
        System.out.println("Sort..using Lambda Function");
        member.stream().sorted((a,b) -> {
            return a.getHeight() - b.getHeight();
        }).forEach(p -> System.out.println(p.getName()));

        //using Compareto Function
        System.out.println("Sort..using CompareTo Function");
        member.stream().sorted((p1, p2) -> {
            return p1.getName().compareTo(p2.getName());
        }).forEach(p -> System.out.println(p.getName()));

    }
}