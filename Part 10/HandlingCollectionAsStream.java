/*
Code by: @Dwarka prasad Bairwa
Content :
        - Handling collection as Streams
 */
import java.util.*;
import java.util.stream.Collectors;

class Person {
    private String firstName;
    private String lastName;
    private int birthYear;

    //constructor
    public Person(String firstName, String lastName, int birthYear) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthYear = birthYear;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public int getBirthYear() {
        return this.birthYear;
    }
}

public class HandlingCollectionAsStream{
    public static void main(String[] args){

        ArrayList<String> inputs = new ArrayList<>();

        //adding values
        inputs.add("5");
        inputs.add("6");
        inputs.add("8");
        inputs.add("3");
        inputs.add("1");
        inputs.add("7");

        //counting the number of values divisible by three
        long numbersDivisibleByThree = inputs.stream()
                .mapToInt(s -> Integer.valueOf(s))
                .filter(number -> number % 3 == 0)
                .count();

        // working out the average
        double average = inputs.stream()
                .mapToInt(s -> Integer.valueOf(s))
                .average()
                .getAsDouble();

        // printing out the statistics
        System.out.println("Divisible by three " + numbersDivisibleByThree);
        System.out.println("Average number: " + average);

        ArrayList<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(7);
        list.add(4);
        list.add(2);
        list.add(6);

        //with integers
        ArrayList<Integer> values = list.stream()
                .filter(s -> s>5)
                .map(s -> s*2)
                .collect(Collectors.toCollection(ArrayList::new));

        for(Integer val:values){
            System.out.println(val);
        }

        //using Foreach along
        list.stream().forEach(s -> System.out.println(s));

        //Reduce method (calculating sum)
        int sum = list.stream()
                .reduce(0,(presum,value) -> presum+value);
        System.out.println("Sum is: "+ sum);

        //sorting list
        list.stream()
                .distinct()
                .sorted()
                .forEach(s -> System.out.println(s));


        //object and Stream
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(new Person("Dwarka","Prasad",2000 ));
        persons.add(new Person("Ravi","Prasad",2005 ));
        persons.add(new Person("Jay","Yadav",1968 ));

        //counting According to birthyear
        long count = persons.stream()
                .filter(person -> person.getBirthYear() < 1970)
                .count();
        System.out.println("Count: " + count);

        //sorting According to Name
        persons.stream()
                .map(person -> person.getFirstName())
                .distinct()
                .sorted()
                .forEach(name -> System.out.println(name));
    }
}