/*
Code by: @Dwarka prasad Bairwa
Content :
        - HashMap
 */
import java.util.HashMap;
import java.util.Scanner;

class Book {
    private String name;
    private String content;
    private int published;

    public Book(String name, int published, String content) {
        this.name = name;
        this.published = published;
        this.content = content;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPublished(int published) {
        this.published = published;
    }

    public String toString() {
        return "Name: " + this.name + " (" + this.published + ")\n"
                + "Content: " + this.content;
    }
}

public class hashMap{
    public static void main(String[] args){

        //creating hashMap
        HashMap<Integer,String> hash = new HashMap<>();

        //adding to hashMap
        hash.put(1,"dwarka");
        hash.put(2,"ravi");
        hash.put(3,"Sachin");

        //retrieving info from hash
        System.out.println(hash.get(1));
        System.out.println(hash.get(2));
        System.out.println(hash.get(10)); //null return cz no entry

        //inserting values for Same keys
        hash.put(2,"Rahul");
        System.out.println(hash.get(2)); //vanish previous value

        //object as a hash value
        Book senseAndSensibility = new Book("Sense and Sensibility", 1811, "...");
        Book prideAndPrejudice = new Book("Pride and Prejudice", 1813, "....");

        HashMap<String, Book> directory = new HashMap<>();

        directory.put(senseAndSensibility.getName(), senseAndSensibility);
        directory.put(prideAndPrejudice.getName(), prideAndPrejudice);

        //iterating in hashMap keys
        for(String name:directory.keySet()){
            System.out.println(directory.get(name));
        }

        //iterating through hashmap values
        for(Book book:directory.values()){
            System.out.println(book);
        }

        //contains in HashMap
        for(String name:directory.keySet()){
            if(directory.containsKey("Pride and Prejudice")) {
                System.out.println(directory.get(name));
            }
        }
    }
}