/*
Code by: @Dwarka prasad Bairwa
Content :
        - grouping using HashMap
 */
import java.util.*;

public class GroupHashMap{
    public static void main(String[] args){

        //hashMap and ArrayList
        HashMap<String, ArrayList<String>> phoneNumbers = new HashMap<>();

        phoneNumbers.put("Dwarka", new ArrayList<>());
        phoneNumbers.get("Dwarka").add("040-12348765");
        phoneNumbers.get("Dwarka").add("09-111333");
        phoneNumbers.get("Dwarka").add("098-823456");

        phoneNumbers.put("Ravi", new ArrayList<>());
        phoneNumbers.get("Ravi").add("040-12348765");
        phoneNumbers.get("Ravi").add("09-111333");

        for(String name:phoneNumbers.keySet()){
            System.out.println(name + ": "+ phoneNumbers.get(name));
        }

    }
}