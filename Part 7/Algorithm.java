/*
Code by: @Dwarka prasad Bairwa

Content :
        - Algorithms

 */
import java.util.Collections;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;

public class Algorithm{
    public static void main(String[] args){

        /* -- Sorting -- */

        //sorting Array
        int[] number = {8,3,7,9,1,2,4};
        System.out.println(Arrays.toString(number));
        Arrays.sort(number);
        System.out.println(Arrays.toString(number));

        //sorting ArrayList
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(8);
        numbers.add(3);
        numbers.add(7);
        System.out.println(numbers);
        Collections.sort(numbers);
        System.out.println(numbers);

        /* --Info Retrieval-- */

        //Binary Search
        int intArr[] = {10, 20, 15, 22, 35};
        Arrays.sort(intArr);
        int intKey = 22;
        System.out.println(intKey + " found at index = " + Arrays.binarySearch(intArr, intKey));

    }
}

