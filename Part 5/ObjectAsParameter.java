/*
Code by: @Dwarka prasad Bairwa

Content :
        - When Object is Used As Parameter
        - When Object is used As Object Variable
        - When Object is used As Return Type of a Function
        - When Object is Used in Same class Method as Parameter
        - Object Equality

 */

//creating class
class FullName{
    private String FirstName;
    private String LastName;

    public FullName(String FirstName,String LastName){
        this.FirstName = FirstName;
        this.LastName = LastName;
    }

    @Override
    public String toString(){
        return FirstName + " " + LastName;
    }
}

//Object As Object Variable is Used
//Object of Same Type is Method Parameter
class Person{
    //class variables
    FullName Name;
    int Age;

    //constructors
    public Person(FullName name,int Age){
        this.Name = name;
        this.Age=Age;
    }

    //Method
    public void growOlder() {
        this.Age = this.Age + 1;
    }

    public boolean OlderThan(Person Compared){
        return (this.Age >= Compared.Age);
    }

    //Object As return Type
    public Person Clone(){
        return new Person(this.Name,this.Age);
    }

    //String representation of an object
    public String toString(){
        return this.Name + " " + this.Age;
    }

}

//object As parameter is USED
class AmusementParkRide {
    private String name;
    private int lowestAge;

    public AmusementParkRide(String name, int lowestAge) {
        this.name = name;
        this.lowestAge = lowestAge;
    }

    public boolean allowedToRide(Person person) {
        if (person.Age < this.lowestAge) return false;
        return true;
    }

    public String toString() {
        return this.name + ", minimum Age: " + this.lowestAge;
    }
}

public class ObjectAsParameter{
    public static void main(String[] args){

        //creating object
        FullName Name1 = new FullName("Dwarka","Bairwa");
        FullName Name2 = new FullName("Jay","Panday");

        Person person1 = new Person(Name1,22);
        Person person2 = new Person(Name2,17);

        AmusementParkRide APR = new AmusementParkRide("Water Park",18);

        //check for person 1
        if (APR.allowedToRide(person1)) {
            System.out.println(person1.Name+ " may enter the ride");
        } else {
            System.out.println(person1.Name + " may not enter the ride");
        }

        //check for person 2

        if (APR.allowedToRide(person2)) {
            System.out.println(person2.Name+ " may enter the ride");
        } else {
            System.out.println(person2.Name + " may not enter the ride");
        }

        //printing
        System.out.println(APR);

        //calling olderThan Function
        if(person1.OlderThan(person2)){
            System.out.println("Person 1 is olderThan person 2");
        }else{
            System.out.println("Person 2 is olderThan person 1");
        }

        //object As return Type
        Person person3 = person1.Clone();
        System.out.println(person1);
        System.out.println(person3);

        //object Equality
        System.out.println("Both Objects Are: "+ (person1.equals(person3)?"Same":"Not Same"));
        person1.growOlder();
        System.out.println(person1);
        System.out.println(person3);
        System.out.println("Both Objects Are: "+ (person1.equals(person3)?"Same":"Not Same"));

    }
}