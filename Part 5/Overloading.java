/*
Code by: @Dwarka prasad Bairwa

Content :
        - constructor and method overloading

 */
import java.util.ArrayList;

//creating class
class Person{
    //class variables
    private String Name;
    private int Age;

    //constructors overloading
    public Person(String name){
        this.Name = name;
        this.Age=0;
    }

    public Person(String name,int Age){
        this.Name = name;
        this.Age=Age;
    }


    //Method overloading
    public void growOlder() {
        this.Age = this.Age + 1;
    }

    public void growOlder(int Years) {
        this.Age = this.Age + Years;
    }

    //String representation of an object
    public String toString(){
        return this.Name + " " + this.Age;
    }

}

public class Overloading{
    public static void main(String[] args){

        //creating object
        Person person1 = new Person("Dwraka",18);
        Person person2 = new Person("Jay");

        //printing data
        System.out.println(person1);
        System.out.println(person2);

        //calling method
        person1.growOlder();
        person2.growOlder(22);

        System.out.println(person1);
        System.out.println(person2);
    }
}