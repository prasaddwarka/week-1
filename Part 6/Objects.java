/*
Code by: @Dwarka prasad Bairwa
Content :
        - Object on a list
        - a list as part of an object
 */
import java.util.*;

//class
class Playlist {
    //list part of an object
    private ArrayList<String> songs;

    //constructor
    public Playlist() {
        this.songs = new ArrayList<>();
    }

    //methods
    public void addSong(String song) {
        this.songs.add(song);
    }

    public void removeSong(String song) {
        this.songs.remove(song);
    }

    public void clearPlayList(){
        this.songs.clear();
    }

    public void printSongs() {
        for (String song: this.songs) {
            System.out.println(song);
        }
    }
}

//person class
class Person{
    //class variables
    String Name;
    int Age;

    //constructors
    public Person(String name,int Age){
        this.Name = name;
        this.Age=Age;
    }

    //Method
    public void growOlder() {
        this.Age = this.Age + 1;
    }

    //String representation of an object
    public String toString(){
        return this.Name + " " + this.Age;
    }

}

//object on a List
class AmusementParkRide {
    private String name;
    private int lowestAge;
    ArrayList<Person> riders;

    public AmusementParkRide(String name, int lowestAge) {
        this.name = name;
        this.lowestAge = lowestAge;
        this.riders = new ArrayList<Person>();
    }

    public boolean allowedToRide(Person person) {
        if (person.Age < this.lowestAge) return false;
        this.riders.add(person);
        return true;
    }

    public void removeEveryoneOnRide() {
        this.riders.clear();
    }

    public String toString() {
        String onRide = "";
        for(Person ps:riders){
            onRide = onRide + ps.Name + "\n";
        }
        return this.name + ", minimum Age: " + this.lowestAge + " Rider : " + onRide;
    }
}


public class Objects{
    public static void main(String[] args){

        //creating object
        Playlist list = new Playlist();
        list.addSong("Zara Sa");
        list.addSong("Hamdard");
        list.printSongs();

        //object on the List
        Person person1 = new Person("Dwarka",22);
        Person person2 = new Person("Ravi",17);

        AmusementParkRide APR = new AmusementParkRide("Water Park",18);

        //check for person 1
        if (APR.allowedToRide(person1)) {
            System.out.println(person1.Name+ " may enter the ride");
        } else {
            System.out.println(person1.Name + " may not enter the ride");
        }

        //check for person 2
        if (APR.allowedToRide(person2)) {
            System.out.println(person2.Name+ " may enter the ride");
        } else {
            System.out.println(person2.Name + " may not enter the ride");
        }

        //printing
        System.out.println(APR);
    }
}